import { Ref, InjectionKey } from 'vue';
import { Namespace } from './types';

export const NamespacesKey: InjectionKey<Ref<Namespace[]>> = Symbol( 'WlhNamespaces' );
